# Darwiin News Aggregator App

## Installation

Install `yarn` for your PC if you haven't already. Browse to the root folder of this app and run the below commands:
1. Install all the dependencies
```
yarn
```
2. Edit `.env` file if you need to change backend api url
3. Start the application
```
yarn run start
```
