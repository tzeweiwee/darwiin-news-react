import {
  STORE_NEWS,
} from "../actions/news/actionTypes";

export default (state = {}, action) => {
  switch (action.type) {
    case STORE_NEWS:
      return {
        ...state,
        [action.provider]: action.payload
      };
    default:
      return state;
  }
};
