import loadable from '@loadable/component';

export default [
    {
        path: '/',
        exact: true,
        title: 'Home',
        navbar: true,
        component: () => loadable(() => import('../pages/News/NewsSearch')),
    }
]