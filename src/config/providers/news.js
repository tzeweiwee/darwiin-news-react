export const getNewsProviderRequest = (query, page) => {
  return {
    newsapi: {
      provider: "newsapi",
      query,
      sortBy: "relevancy",
      language: "en",
      resultsCount: 20,
      page,
      additionalData: {
        sources: ""
      }
    },
    bing: {
      provider: "bing",
      query,
      sortBy: "",
      language: "en-SG",
      resultsCount: 22,
      page,
      additionalData: {
        freshness: ""
      }
    },
    contextualnews: {
      provider: "contextualnews",
      query,
      sortBy: "",
      language: "",
      resultsCount: 20,
      page,
      additionalData: {
      }
    }
  };
};
