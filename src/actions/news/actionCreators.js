import {
  START_GET_NEWS,
  STORE_NEWS,
  END_GET_NEWS
} from "./actionTypes";

export const getNewsAction = () => {
  return {
    type: START_GET_NEWS
  };
};

export const storeNewsAction = (payload, provider, page) => {
  return {
    type: STORE_NEWS,
    payload,
    provider,
  };
};

export const endNewsAction = () => {
  return {
    type: END_GET_NEWS
  };
};
