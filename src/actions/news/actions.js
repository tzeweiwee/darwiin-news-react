import axios from "axios";
import {
  getNewsAction,
  storeNewsAction,
  endNewsAction
} from "./actionCreators";
import { getNewsProviderRequest } from "../../config/providers/news";

const callNewsAPI = async (query, provider, page) => {
  try {
    const res = await axios.post(
      `${process.env.REACT_APP_API_URL}/news`,
      getNewsProviderRequest(query, page)[provider]
    );
    return res.data;
  } catch (error) {
    console.log(error);
  }
};

export const getNews = (query, provider, page) => async (dispatch, getState) => {
  dispatch(getNewsAction());
  const response = await callNewsAPI(query, provider, page);

  const { news } = getState();
  let existingArticles = [];
  if(page !== 1 && news[provider] && news[provider].articles) {
    existingArticles = news[provider].articles;
  }
  response.articles = existingArticles.concat(response.articles);

  dispatch(storeNewsAction(response, provider, page));
  dispatch(endNewsAction());
};
