import React from "react";
import styles from "./BackgroundSection.module.scss";

const BackgroundSection = ({title}) => {
  return (<div className={styles.container}>{title}</div>);
};

export default BackgroundSection;