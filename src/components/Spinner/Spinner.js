import React from "react";
import { Spinner as S } from "react-bootstrap";

export const Spinner = () => {
  return (
    <S animation="border" role="status">
      <span className="sr-only">Loading...</span>
    </S>
  );
};
