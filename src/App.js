import React from "react";
import AppRouter from "./components/AppRouter/AppRouter";
import styles from "./App.module.scss";

const App = () => {
  
  return (
    <div className={styles.container}>
      <AppRouter />
    </div>
  );
};


export default App;
