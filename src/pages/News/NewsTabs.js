import React from "react";
import { Row, Tab, Col, Nav } from "react-bootstrap";
import { NewsList } from "./NewsList";
import styles from "./NewsTabs.module.scss";

export const NewsTabs = ({ news, query, getNews }) => {
  const providers = [
    {
      key: "bing",
      value: "Bing"
    },
    { key: "newsapi", value: "NewsAPI" },
    { key: "contextualnews", value: "Contextual News" }
  ];
  const navItems = providers.map((p, i) => (
    <Nav.Item key={i}>
      <Nav.Link eventKey={p.key} className={styles.navLink}>
        {p.value}
        {news[p.key] && <div className={styles.pill}>{news[p.key].totalResults}</div>}
      </Nav.Link>
    </Nav.Item>
  ));
  const navPanels = providers.map((p, i) => (
    <Tab.Pane eventKey={p.key} key={i}>
      {news[p.key] && (
        <NewsList
          totalResults={news[p.key].totalResults}
          provider={p.key}
          getNews={getNews}
          articles={news[p.key].articles}
          query={query}
        />
      )}
    </Tab.Pane>
  ));

  return (
    <Tab.Container defaultActiveKey="bing">
      <Row>
        <Col>
          <Nav variant="tabs">{navItems}</Nav>
        </Col>
      </Row>
      <Row>
        <Col>
          <Tab.Content>{navPanels}</Tab.Content>
        </Col>
      </Row>
    </Tab.Container>
  );
};
