import React from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Button
} from "react-bootstrap";
import styles from "./NewsSearchForm.module.scss";

export const NewsSearchForm = props => {
  const {
    values,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting
  } = props;

  return (
    <Container>
      <Row className={styles.searchRow}>
        <Col md={5}>
          <Form.Control
            type="text"
            placeholder="Company name"
            name={"query"}
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.query}
          />
        </Col>
        <Col md={1}>
          <Button
            disabled={isSubmitting}
            onClick={handleSubmit}
            variant="outline-primary"
          >
            Search
          </Button>
        </Col>
      </Row>
      <Row className={styles.searchRow}>
        <Col md={6}>
          <Form.Check
            className={styles.checkbox}
            type={"checkbox"}
            id={"exact-match"}
            label={"Exact match"}
            name={"exactMatch"}
            defaultChecked={false}
            value={values.exactMatch}
            onChange={handleChange}
          />
        </Col>
      </Row>
    </Container>
  );
};
