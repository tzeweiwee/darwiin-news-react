import React from "react";
import styles from "./NewsArticle.module.scss";


export const NewsArticle = props => {
  const {
    content,
    publishedDate,
    source,
    thumbnailUrl,
    url,
    title,
    query
  } = props;

  const boldQuery = (str, find) => {
    const re = new RegExp(find, "gi");
    if (!str) {
      return "";
    }
    return str.replace(re, q => `<b>${q}</b>`);
  };

  const massageDate = (date) => {
      let newDate = new Date(date);
      return newDate.getDate() + "/" + newDate.getMonth() + "/" + newDate.getFullYear()
  }

  return (
    <div className={styles.container}>
      <div className={styles.imageContainer}>
        <img className={styles.image} src={thumbnailUrl} alt="" />
      </div>
      <div className={styles.infoContainer}>
        <p className={styles.title}>
          <a
            href={url}
            target="_blank"
            rel="noopener noreferrer"
            dangerouslySetInnerHTML={{ __html: boldQuery(title, query) }}
          ></a>
        </p>
        <p className={styles.subtitle}>
          <span className={styles.source}>{source}</span>
          <span className={styles.date}>{massageDate(publishedDate)}</span>
        </p>
        <p
          className={styles.content}
          dangerouslySetInnerHTML={{ __html: boldQuery(content, query) }}
        ></p>
      </div>
    </div>
  );
};

export default NewsArticle;
