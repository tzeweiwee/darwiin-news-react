import React from "react";
import { Container, Button } from "react-bootstrap";
import NewsArticle from "./NewsArticle";
import styles from "./NewsList.module.scss";

export const NewsList = ({ totalResults, provider, articles, query, getNews }) => {
  return (
    <Container>
      <p className={styles.helper}>showing {articles.length} of {totalResults}</p>
      {articles.map((a, i) => {
        return <NewsArticle key={i} {...a} query={query} />;
      })}
      <div className={styles.paginationLink}>
        <Button variant="link" onClick={() => {getNews(provider, query)}}>Load more news</Button>
      </div>
    </Container>
  );
};
