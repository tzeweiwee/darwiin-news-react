import React, { Component } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import * as Yup from "yup";
import { Row, Col, Container } from "react-bootstrap";
import * as newsActions from "../../actions/news/actions";
import BackgroundSection from "../../components/BackgroundSection/BackgroundSection";
import styles from "./NewsSearch.module.scss";
import { NewsSearchForm } from "./NewsSearchForm";
import { NewsTabs } from "./NewsTabs";
import { Spinner } from "../../components/Spinner/Spinner";

class NewsSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: ""
    };
    this.handleQuery = this.handleQuery.bind(this);
    this.loadMoreNews = this.loadMoreNews.bind(this);
  }

  handleQuery(query) {
    this.setState({ query });
  }

  loadMoreNews(provider, query) {
    try {
      const {
        getNews,
        news: {
          [provider]: { page }
        }
      } = this.props;
      getNews(query, provider, page + 1);
    } catch (err) {
      console.error(err.message);
    }
  }

  render() {
    const { getNews, news } = this.props;

    return (
      <>
        <BackgroundSection title={"Darwiin News Search"} />
        <Formik
          initialValues={{ query: "", exactMatch: false }}
          validationSchema={Yup.object().shape({
            query: Yup.string().required("Required")
          })}
          onSubmit={(values, { setSubmitting }) => {
            let query = values.query;
            if(values.exactMatch) {
              query = `"${query}"`;
            }
            getNews(query, "newsapi", 1);
            getNews(query, "bing", 1);
            getNews(query, "contextualnews", 1);
            setSubmitting(false);
            this.handleQuery(values.query);
          }}
        >
          {props => {
            return (
              <>
                <div className={styles.formContainer}>
                  <NewsSearchForm {...props} />
                </div>
                {props.isSubmitting && <Spinner />}
              </>
            );
          }}
        </Formik>
        <Container>
          <Row className={styles.tabsContainer}>
            <Col md={12}>
              <NewsTabs
                news={news}
                query={this.state.query}
                getNews={this.loadMoreNews}
              />
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    ...state
  };
};

const mapDispatchToProps = dispatch => ({
  getNews: (query, provider, page) =>
    dispatch(newsActions.getNews(query, provider, page))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsSearch);
